require 'rails_helper'

RSpec.describe NotesController, :type => :request do

  it 'GET show' do
    get '/notes/1', format: :json

    expect(json).to have_key 'title'
    expect(json).to have_key 'content'
  end

end
